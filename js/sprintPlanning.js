"use strict";

function sprintPlanning(speedArr, backlogArr, deadline) {
    let today = new Date(2022, 2, 9); //change to "new Date()" and change console input in case that you will check homework later than 3/9/2022

    let workDayHours = 8;

    let backLogSum = sumPoints(backlogArr);
    let teamProductivity = sumPoints(speedArr);

    let workingDaysNeeded = (backLogSum / teamProductivity).toFixed(2);
    let workingDaysAvailableToDate = availableWorkingDays(today, deadline);

    if (workingDaysNeeded <= workingDaysAvailableToDate) {
        return `All tasks would be completed ${((workingDaysAvailableToDate - workingDaysNeeded)).toFixed(1)} working days before deadline`;
    } else {
        return `Team of developers needs extra ${((workingDaysNeeded - workingDaysAvailableToDate) * workDayHours).toFixed(1)} working hours to finish tasks`;
    }
}

function sumPoints(arr) {
    return arr.reduce((previousValue, currentValue) => {
        return previousValue + currentValue;
    })
}

function availableWorkingDays(startDate, endDate) {
    let totalDaysAvailable = ((endDate - startDate) / (24 * 3600 * 1000)).toFixed(2);
    let start = sundayChange(startDate.getDay());
    let end = sundayChange(endDate.getDay());
    let workingDays = 0;


    if (totalDaysAvailable >= 7) {
        workingDays = (totalDaysAvailable / 7) * 5 + totalDaysLessThanSeven();
    } else {
        return totalDaysLessThanSeven();
    }

    function sundayChange(num) {
        if (num === 0) {
            return 7;
        }
        return num;
    }

    function totalDaysLessThanSeven() {
        let diff = 0;
        if (totalDaysAvailable % 7 === 0) {
            return 0;
        }

        if (end > start) {
            if (end === 6 || end === 7) {
                diff = end === 6 ? (end - start + 1) - 1 : (end - start + 1) - 2; // +1 (excluded) -1 (for Sunday) -2 (for Saturday)
            } else {
                diff = end - start + 1;
            }
        } else {
            if (start === 6 || start === 7) {
                diff = end === 6 ? (-(end - start) + 1) - 2 : (-(end - start) + 1) - 1; // +1 (excluded) -1 (for Sunday) -2 (for Saturday)
            } else
                diff = -(end - start) + 1;
        }
        return diff;
    }

    return workingDays;
}


console.log(sprintPlanning([4, 6], [11, 9, 20, 9], new Date(2022, 2, 16)));
console.log(sprintPlanning([4, 6], [11, 9, 20, 9], new Date(2022, 2, 14)));